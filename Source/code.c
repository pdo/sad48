/* code.c -- SAD machine code disassembler programs.

This file is part of SAD, the Saturn Disassembler package.

SAD is not distributed by the Free Software Foundation. Do not ask
them for a copy or how to obtain new releases. Instead, send e-mail to
the address below. SAD is merely covered by the GNU General Public
License.

Please send your comments, ideas, and bug reports to
Jan Brittenson <bson@ai.mit.edu>

*/


/* Copyright (C) 1990 Jan Brittenson.

SAD is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 1, or (at your option) any later
version.

SAD is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with SAD; see the file COPYING.  If not, write to the Free
Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA. */


#include <stdio.h>
#include <string.h>
#include "globals.h"
#include "formats.h"

static char     
     *fixed_0e[] = {
	   "A=A&B", "B=B&C", "C=C&A", "D=D&C",
	   "B=B&A", "C=C&B", "A=A&C", "C=C&D",
	   "A=A!B", "B=B!C", "C=C!A", "D=D!C",
	   "B=B!A", "C=C!B", "A=A!C", "C=C!D"  },
     
     *std_fields[] = {
	   "P", "WP", "XS", "X", "S", "M", "B", "W",
	   "P", "WP", "XS", "X", "S", "M", "B", "A" },
     
     *fixed_f1[] = {
	   "A", "A", "A", "A", "A", "A", "A", "A",
	   "C", "C", "C", "C", "C", "C", "C", "C"
	   },
     
     *fixed_rf1[] = {
	   "0", "1", "2", "3", "4", "1", "2", "3",
	   "0", "1", "2", "3", "4", "1", "2", "3"
	   };	

static void jumpify(loc, name, str)
    int loc;
    char *name, *str;
{
     static char nbuf[132];
     
     if(opt_jumpify)
      {
	   if(pass!=PASS1)
		return;
	   if(strncmp(name, "L_",2)==0)
		return;
	   if(*name == '=')
		name++;
	   sprintf(nbuf, "L_%s", name);
	   add_local_name(loc, nbuf, T_SYM_LNAME);
      }
     else
	  force_comment(str, name);
}


/* Does location contain RPL exit sequence? */
static int is_rplloop(loc)
    int loc;
{
     if((fetch_unibbles(loc+0,3)==0x241) &	/* A=DAT0 A */
	(fetch_unibbles(loc+3,3)==0x461) &	/* D0=D0+ 5 */
	(fetch_unibbles(loc+6,4)==0xC808))	/* PC=(A)   */
	  return(TRUE);
     else
	  return(FALSE);
}



/* Look ahead for jumping commands to comment possible new jumps */
/* Saves space not having to list all GOVLNG =SAVPTR etc commands in symbols */
static void comment_jump(loc)
    int loc;
{
     static char nbuf[132];

     int instr1, instr2, instr3, offs;
     
     if((loc < baseaddr) || (loc > coresize + baseaddr))
	  return;
     
     jumpaddr=loc;
     
     if(fetch_unibbles(loc,1)==0x6)		/* GOTO x */
      {
	   jumpaddr = (fetch_nibbles(loc+1,3)+loc+1) & 0xfffff;
	   jumpify(loc, symbolic(jumpaddr,NOREL,SCODE), "GOTO %s");
	   return;
      }

     switch(fetch_unibbles(loc,2))
      {
      case 0xC8:		/* GOLONG */
	   jumpaddr = (fetch_nibbles(loc+2,4)+loc+2) & 0xfffff;
	   jumpify(loc, symbolic(jumpaddr,NOREL,SCODE), "GOLONG %s");
	   return;
      case 0xD8:		/* GOVLNG */
	   jumpaddr = fetch_unibbles(loc+2,5);
	   jumpify(loc, symbolic(jumpaddr,NOREL,SRPL), "GOVLNG %s");
	   return;
      case 0x00:		/* RTNSXM */
	   jumpify(loc, "RTNSXM", "%s");
	   return;
      case 0x10:		/* RTN */
	   jumpify(loc, "RTN", "%s");
	   return;
      case 0x20:		/* RTNSC */
	   jumpify(loc, "RTNSC", "%s");
	   return;
      case 0x30:		/* RTNCC */
	   jumpify(loc, "RTNCC", "%s");
	   return;
      default:
	   break;
      }

     if(is_rplloop(loc))
      {
	   jumpify(loc, "LOOP", "%s");
	   return;
      }
     
     if(is_rplloop(loc+3))
	  switch(fetch_unibbles(loc,3))
	   {
	   case 0x031:				/* D0=A     */
		jumpify(loc, "D0=ALOOP", "%s");
		return;
	   case 0x131:				/* D1=A     */
		jumpify(loc, "D1=ALOOP", "%s");
		return;
	   case 0x231:				/* AD0EX    */
		jumpify(loc, "AD0EXLOOP", "%s");
		return;
	   case 0x331:				/* AD1EX    */
		jumpify(loc, "AD1EXLOOP", "%s");
		return;
	   case 0x431:				/* D0=C     */
		jumpify(loc, "D0=CLOOP", "%s");
		return;
	   case 0x551:				/* D1=C     */
		jumpify(loc, "D1=CLOOP", "%s");
		return;
	   case 0x631:				/* CD0EX    */
		jumpify(loc, "CD0EXLOOP", "%s");
		return;
	   case 0x731:				/* CD1EX    */
		jumpify(loc, "CD1EXLOOP", "%s");
		return;
	   case 0x141:				/* DAT1=A A */
		jumpify(loc, "OverWrALp", "%s");
		return;
	   case 0x441:				/* DAT1=C A */
		jumpify(loc, "OverWrCLp", "%s");
		return;
	   default:
		break;
	   }

     if(fetch_unibbles(loc,2)==0x80)		/* C=RSTK */
	  if(is_rplloop(loc+5))
	       switch(fetch_unibbles(loc+2,3))
		{
		case 0x431:				/* D0=C     */
		     jumpify(loc, "D0=RSTKLOOP", "%s");
		     return;
		case 0x551:				/* D1=C     */
		     jumpify(loc, "D1=RSTKLOOP", "%s");
		     return;
		case 0x631:				/* CD0EX    */
		     jumpify(loc, "RSTKD0EXLOOP", "%s");
		     return;
		case 0x731:				/* CD1EX    */
		     jumpify(loc, "RSTKD1EXLOOP", "%s");
		     return;
		case 0x441:				/* DAT1=C A */
		     jumpify(loc, "OverWrRstkLp", "%s");
		     return;
		default:
		     break;
	   }
     
     
     if((fetch_unibbles(loc,2)==0x43) &		/* LC(5) x  */
	(fetch_unibbles(loc+7,2)==0xAD) &	/* A=C   A  */
	(fetch_unibbles(loc+9,4)==0xC80C))	/* PC=(A)   */
      {
	   offs=fetch_unibbles(loc+2,5);
	   sprintf(nbuf, "EXIT_%s", symbolic(offs, NORELRPL, SRPL));
	   jumpify(loc, nbuf, "%s");
	   return;
      }

     if((fetch_unibbles(loc,2)==0x8F) &		/* GOSBVL x */
	(is_rplloop(loc+7)))			/* LOOP     */
      {
	   sprintf(nbuf,"%s+Lp",symbolic(fetch_unibbles(loc+2,5),NOREL,SCODE));
	   jumpify(loc, nbuf, "%s");
	   return;
      }
}


/* Decode 80x instruction */
static void decode_group_80()
{
     static char *i807[] = {
	   "OUT=CS", "OUT=C", "A=IN", "C=IN",
	   "UNCNFG", "CONFIG", "C=ID", "SHUTDN" };
     
     int op3, op4, op5, op6;
     
     op3 = get_unibbles(1);
     if(op3 < 8)
      {
	   strcpy(l_instr, i807[op3]);
	   return;
      }
     
     switch(op3)
      {
      case 9:   strcpy( l_instr, "C+P+1"); break;
      case 0xa: strcpy( l_instr, "RESET"); break;
      case 0xb: strcpy( l_instr, "BUSCC"); break;
      case 0xc: sprintf(l_instr, "C=P     %d", get_unibbles(1)); break;
      case 0xd: sprintf(l_instr, "P=C     %d", get_unibbles(1)); break;
      case 0xe: strcpy( l_instr, "SREQ?"); break;
      case 0xf: sprintf(l_instr, "CPEX    %d", get_unibbles(1)); break;
	   
      case 8:
	   
	   op4 = get_unibbles(1);
	   switch(op4)
	    {
	    case 0: strcpy(l_instr, "INTON"); break;
	    case 3: strcpy(l_instr, "BUSCB"); break;
	    case 4: sprintf(l_instr, "ABIT=0  %d",get_unibbles(1)); break;
	    case 5: sprintf(l_instr, "ABIT=1  %d",get_unibbles(1)); break;
	    case 8: sprintf(l_instr, "CBIT=0  %d",get_unibbles(1)); break;
	    case 9: sprintf(l_instr, "CBIT=1  %d",get_unibbles(1)); break;
	    case 0xc: strcpy(l_instr, "PC=(A)"); break;
	    case 0xd: strcpy(l_instr, "BUSCD"); break;
	    case 0xe: strcpy(l_instr, "PC=(C)"); break;
	    case 0xf: strcpy(l_instr, "INTOFF"); break;
	    case 1:
		 sprintf(l_instr, "RSI%s", (get_unibbles(1) ? "*" : ""));
		 break;
	    case 2:
		 op5 = get_unibbles(1);
		 if(op5>4)
		      sprintf(l_instr, "LAHEX   %s",make_hexstr_rev(op5+1));
		 else
		  {
		       op6 = get_unibbles(op5+1);
			    
		       if(op5==4)
			    if(last_cpc == org_pc-4)
			     {
				  add_local_symbol(org_pc+op6,T_SYM_NOHP);
				  sprintf(l_instr, "LA(5)   (%s)-(*)",
					  symbolic(org_pc+op6,DATAREL, SCODE));
			     }
			    else
				 sprintf(l_instr,"LA(5)   %s",
					 cksymbolic(op6, DATAREL, SCODE));
		       else
			    sprintf(l_instr,"LA(%d)   #%X",op5+1, op6);
		       
		       if( (op5 == 1) && (op6 >= ' ') && ( op6 <= 127 ))
			    force_comment("LAASC '%c'", op6);
		  }
		 break;
	    case 6:
	    case 7:
	    case 0xa:
	    case 0xb:
		 op5 = get_unibbles(1);
		 branch=TRUE;
		 sprintf(l_instr, "?%cBIT=%d %d",
			 (op4<8) ? 'A' : 'C',
			 ( (op4==6) || (op4==0xa)) ? 0 : 1,
			 op5);
		 break;
	    }
      }
}



	  
/* Decode instructions starting with 8-f */
static void decode_8_thru_f(op1)
   int op1;
{
     int op, op2, op3, op4, op5, op6;
     static char *fixed_81[]  = {
	   "ASLC", "BSLC", "CSLC", "DSLC",
	   "ASRC", "BSRC", "CSRC", "DSRC",
	   "", "", "", "",
	   "ASRB", "BSRB", "CSRB", "DSRB" },
     
     *fixed_81b[] = {
	   "!81B0?", "!81B1?", "PC=A", "PC=C",
	   "A=PC", "C=PC", "APCEX", "CPCEX",
	   "!81B8?", "!81B9?", "!81BA?", "!81BB?",
	   "!81BC?", "!81BD?", "!81BE?", "!81BF?" },
     
     *fixed_8a9[] = {
	   "?A=B", "?B=C", "?A=C", "?C=D",
	   "?A#B", "?B#C", "?A#C", "?C#D",
	   "?A=0", "?B=0", "?C=0", "?D=0",
	   "?A#0", "?B#0", "?C#0", "?D#0" },

     *fixed_8b9[] = {
	   "?A>B ", "?B>C ", "?C>A ", "?D>C ",
	   "?A<B ", "?B<C ", "?C<A ", "?D<C ",
	   "?A>=B", "?B>=C", "?C>=A", "?D>=C",
	   "?A<=B", "?B<=C", "?C<=A", "?D<=C" },

     *fixed_c[] = {
	   "A=A+B", "B=B+C", "C=C+A", "D=D+C",
	   "A=A+A", "B=B+B", "C=C+C", "D=D+D",
	   "B=B+A", "C=C+B", "A=A+C", "C=C+D",
	   "A=A-1", "B=B-1", "C=C-1", "D=D-1" },

     *fixed_d[] = {
	   "A=0 ", "B=0 ", "C=0 ", "D=0 ",
	   "A=B ", "B=C ", "C=A ", "D=C ",
	   "B=A ", "C=B ", "A=C ", "C=D ",
	   "ABEX", "BCEX", "ACEX", "CDEX" },

     *fixed_e[] = {
	   "A=A-B", "B=B-C", "C=C-A", "D=D-C",
	   "A=A+1", "B=B+1", "C=C+1", "D=D+1",
	   "B=B-A", "C=C-B", "A=A-C", "C=C-D",
	   "A=B-A", "B=C-B", "C=A-C", "D=C-D" },

     *fixed_f[] = {
	   "ASL   ", "BSL   ", "CSL   ", "DSL   ",
	   "ASR   ", "BSR   ", "CSR   ", "DSR   ",
	   "A=-A  ", "B=-B  ", "C=-C  ", "D=-D  ",
	   "A=-A-1", "B=-B-1", "C=-C-1", "D=-D-1" };
     
     op2 = get_unibbles(1);

     switch(op1) {
      case 8:
	   switch(op2)
	    {
	    case 0: decode_group_80(); return;
	    case 1:
		 op3 = get_unibbles(1);
		 if( (op3<=7) || (op3>=0xC))
		  {
		       sprintf(l_instr, "%s", fixed_81[op3]);
		       return;
		  }
		 op4 = get_unibbles(1);
		 switch(op3)
		  {
		  case 8:
		       op5 = get_unibbles(1);
		       op6 = get_unibbles(1);
		       sprintf(l_instr, "%c=%c%cCON %s,%d",
			       ('A'+(op5&3)),
			       ('A'+(op5&3)),
			       (op5<8) ? '+' : '-',
			       std_fields[op4],
			       op6+1);
		       return;
		  case 9:
		       op5 = get_unibbles(1);
		       sprintf(l_instr, "%cSRB.F  %s",
			       ('A'+(op5&3)),
			       std_fields[op4]);
		       return;
		  case 0xa:
		       op5 = get_unibbles(1);
		       op6 = get_unibbles(1);
		       switch(op5)
			{
			case 0:
			     sprintf(l_instr, "R%s=%s.F  %s",
				     fixed_rf1[op6],
				     fixed_f1[op6],
				     std_fields[op4]);
			     return;
			case 1:
			     sprintf(l_instr, "%s=R%s.F  %s",
				     fixed_f1[op6],
				     fixed_rf1[op6],
				     std_fields[op4]);
			     return;
			case 2:
			     sprintf(l_instr, "%sR%sEX.F %s",
				     fixed_f1[op6],
				     fixed_rf1[op6],
				     std_fields[op4]);
			     return;
			default:
			     sprintf(l_instr, "???.F    %s",std_fields[op4&7]);
			     return;
			}
		  case 0xb:
		       sprintf(l_instr, "%s", fixed_81b[op4]);
		       switch(op4)
			{
			case 0x4: last_apc = org_pc; break;
			case 0x5: last_cpc = org_pc; break;
			default: break;
			}
		       return;
		  }
		 return;
	    case 2:
		 op3=get_unibbles(1);
		 switch(op3)
		  {
		  case 1:
		       sprintf(l_instr, "XM=0");
		       return;
		  case 2:
		       sprintf(l_instr, "SB=0");
		       return;
		  case 4:
		       sprintf(l_instr, "SR=0");
		       return;
		  case 8:
		       sprintf(l_instr, "MP=0");
		       return;
		  case 0xF:
		       sprintf(l_instr, "CLRHST");
		       return;
		  default:
		       sprintf(l_instr, "HS=0    %d", op3);
		      return;
		  }
	    case 3:
		 op3 = get_unibbles(1);
		 branch=TRUE;
		 switch(op3)
		  {
		  case 1:
		       sprintf(l_instr, "?XM=0");
		       return;
		  case 2:
		       sprintf(l_instr, "?SB=0");
		       return;
		  case 4:
		       sprintf(l_instr, "?SR=0");
		       return;
		  case 8:
		       sprintf(l_instr, "?MP=0");
		       return;
		  default:
		       sprintf(l_instr, "?HS=0   %d", op3);
		       return;
		  }
	    case 4:
	    case 5:
		 op3 = get_unibbles(1);
		 sprintf(l_instr, "ST=%d    %d",
			 (op2 == 4) ? 0 : 1 ,
			 op3);
		 return;
	    case 6:
	    case 7:
		 op3 = get_unibbles(1);
		 branch=TRUE;
		 sprintf(l_instr, "?ST=%d   %d",(op2==6) ? 0 : 1, op3);
		 return;
	    case 8:
	    case 9:
		 op3 = get_unibbles(1);
		 branch=TRUE;
		 sprintf(l_instr, "?P%c     %d",(op2==8) ? '#' : '=', op3);
		 return;
	    case 0xa:
		 op3 = get_unibbles(1);
		 branch=TRUE;
		 sprintf(l_instr, "%s    A", fixed_8a9[op3]);
		 return;
	    case 0xb:
		 op3 = get_unibbles(1);
		 branch=TRUE;
		 sprintf(l_instr, "%s   A", fixed_8b9[op3]);
		 return;
	    case 0xc:
		 op3 = get_nibbles(4);
		 sprintf(l_instr, "GOLONG  %s",
			 symbolic((org_pc + 2 + op3) & 0xfffff, JUMPREL, SCODE));
		 comment_jump(org_pc+2+op3);
		 if(opt_formats && pass == PASSF)
		      add_auto_format(org_pc + 2 + op3, T_FMT_CODE);
		 return;
	    case 0xd:
		 op3 = get_unibbles(5);
		 sprintf(l_instr, "GOVLNG  %s", symbolic(op3, JUMPREL, SCODE));
		 comment_jump(op3);
		 if(opt_formats && pass == PASSF)
		     add_auto_format(op3, T_FMT_CODE);
		 return;
	    case 0xe:
		 op3 = get_nibbles(4);
		 sprintf(l_instr, "GOSUBL  %s",
			 symbolic((org_pc + 6 + op3) & 0xfffff,
				  JUMPREL,SCODE));
		 gosub=TRUE;
		 comment_jump(org_pc+6+op3);
		 if(opt_formats && pass == PASSF)
		      add_auto_format(org_pc + 6 + op3, T_FMT_CODE);
		 return;
	    case 0xf:
		 op3 = get_unibbles(5);
		 sprintf(l_instr, "GOSBVL  %s", symbolic(op3, JUMPREL, SCODE));
		 gosub=TRUE;
		 comment_jump(op3);
		 if(opt_formats && pass == PASSF)
		      add_auto_format(op3, T_FMT_CODE);
		 return;
	    }
	   return;
      case 9:
	   op3 = get_unibbles(1);
	   branch=TRUE;
	   if(op2<8)
		sprintf(l_instr, "%s    %s", fixed_8a9[op3], std_fields[op2]);
	   else
		sprintf(l_instr, "%s   %s", fixed_8b9[op3], std_fields[op2&7]);
	   return;
      case 0xa:
	   op3 = get_unibbles(1);
	   if(op2<8)
		sprintf(l_instr, "%s   %s", fixed_c[op3], std_fields[op2]);
	   else
		sprintf(l_instr, "%s    %s", fixed_d[op3], std_fields[op2&7]);
	   return;
      case 0xb:
	   op3 = get_unibbles(1);
	   if(op2<8)
		sprintf(l_instr, "%s   %s", fixed_e[op3], std_fields[op2]);
	   else
		sprintf(l_instr, "%s  %s", fixed_f[op3], std_fields[op2&7]);
	   return;
      case 0xc:
	   sprintf(l_instr, "%s   A", fixed_c[op2]);
	   return;
      case 0xd:
	   sprintf(l_instr, "%s    A", fixed_d[op2]);
	   return;
      case 0xe:
	   sprintf(l_instr, "%s   A", fixed_e[op2]);
	   return;
      case 0xf:
	   sprintf(l_instr, "%s  A", fixed_f[op2]);
	   return;
      }
}

      
/* Decode instructions starting with '1' */
static void decode_group_1()
{
     int op2, op3, op4;
     
     static char *cmd_13[] = {
	   "D0=A", "D1=A", "AD0EX", "AD1EX",
	   "D0=C", "D1=C", "CD0EX", "CD1EX",
	   "D0=AS", "D1=AS", "AD0XS", "AD1XS",
	   "D0=CS", "D1=CS", "CD0XS", "CD1XS" },

     *cmd_1415[] = {
	   "DAT0=A", "DAT1=A", "A=DAT0", "A=DAT1",
	   "DAT0=C", "DAT1=C", "C=DAT0", "C=DAT1" };
     
     op2 = get_unibbles(1);
     if(op2 < 8)
	  op3 = get_unibbles(1);
     switch(op2)
      {
      case 0:
	   sprintf(l_instr, "R%s=%s", fixed_rf1[op3], fixed_f1[op3]);
	   break;
	   
      case 1:
	   sprintf(l_instr, "%s=R%s", fixed_f1[op3], fixed_rf1[op3]);
	   break;
	   
      case 2:
	   sprintf(l_instr, "%sR%sEX", fixed_f1[op3], fixed_rf1[op3]);
	   break;
	   
      case 3:
	   sprintf(l_instr, "%s", cmd_13[op3]);
	   break;
	   
      case 4:
	   sprintf(l_instr, "%s  %c", cmd_1415[op3&7], (op3<8) ? 'A' : 'B' );
	   break;
	   
      case 5:
	   op4 = get_unibbles(1);
	   if(op3 >= 8)
		sprintf(l_instr, "%s  %d", cmd_1415[op3&7], op4+1);
	   else
		sprintf(l_instr, "%s  %s", cmd_1415[op3], std_fields[op4]);
	   break;
	   
      case 6: sprintf(l_instr, "D0=D0+  %d", op3+1); break;
      case 7: sprintf(l_instr, "D1=D1+  %d", op3+1); break;
      case 8: sprintf(l_instr, "D0=D0-  %d", get_unibbles(1)+1); break;
      case 9: 
	   op4 = get_unibbles(2);	/* Just data */
	   if(opt_comref)
		if(op4<=0x38)
		     sprintf(l_instr,"D0=(2)  %s",
			     symbolic(0x00100+op4,DATAREL, SCODE));
		else
		     sprintf(l_instr,"D0=(2)  %s",
			     symbolic(ram_base+op4,DATAREL, SCODE));
	   else
		sprintf(l_instr, "D0=(2)  %s",addrtohex(op4));
	   break;
	   
      case 0xa:
	   if(opt_comref)
		sprintf(l_instr,"D0=(4)  %s",
			symbolic(ram_base+get_unibbles(4),DATAREL, SCODE));
	   else
		sprintf(l_instr, "D0=(4)  %s",addrtohex(get_unibbles(4)));
	   break;
      case 0xb:
	   sprintf(l_instr, "D0=(5)  %s",
		   cksymbolic(get_unibbles(5), DATAREL, SCODE));
	   break;
	   
      case 0xc: sprintf(l_instr, "D1=D1-  %d", get_unibbles(1)+1); break;
      case 0xd: 
	   op4=get_unibbles(2);		/* Just data */
	   if(opt_comref)
		if(op4<=0x38)
		     sprintf(l_instr,"D1=(2)  %s",
			     symbolic(0x00100+op4,DATAREL, SCODE));
		else
		     sprintf(l_instr,"D1=(2)  %s",
			     symbolic(ram_base+op4,DATAREL, SCODE));
	   else
		sprintf(l_instr, "D1=(2)  %s",addrtohex(op4));
	   break;
      case 0xe:
	   if(opt_comref)
		sprintf(l_instr,"D1=(4)  %s",
			symbolic(ram_base+get_unibbles(4),DATAREL, SCODE));
	   else
		sprintf(l_instr, "D1=(4)  %s",addrtohex(get_unibbles(4)));
	   break;
	   
      case 0xf:
	   sprintf(l_instr, "D1=(5)  %s",
		   cksymbolic(get_unibbles(5), DATAREL, SCODE));
	   break;
      }
}


/* Decode one instruction */
void decode_instr()
{
     int op0, op1, op2, op3;
     
     static char
	  *fixed_0[] = 
	   {
		"RTNSXM", "RTN", "RTNSC", "RTNCC",
		"SETHEX", "SETDEC", "RSTK=C", "C=RSTK",
		"CLRST", "C=ST", "ST=C", "CSTEX",
		"P=P+1", "P=P-1", "***", "RTI"
		};
     
     jumpaddr=0;
     gosub=FALSE;
     if(branch)
      {
	   branch=FALSE;
	   op0=get_nibbles(2);

//	   sprintf(l_instr,
//		   (op0 ? "GOYES   %s" : "RTNYES"),
//		   (op0 ? (char *) symbolic((org_pc + op0) & 0xfffff, JUMPREL, SCODE) : "*"));
// code above replaced by following if-else

	   if (op0)
                sprintf(l_instr,"GOYES   %s", symbolic((org_pc + op0) & 0xfffff, JUMPREL, SCODE));
	   else
		sprintf(l_instr,"RTNYES");

	   if(op0)
		comment_jump(org_pc+op0);
	   if(opt_formats && op0 && pass == PASSF)
		add_auto_format(org_pc + op0,T_FMT_CODE);
	   return;
      }
     
     op0=get_unibbles(1);
     switch(op0)
      {
      case 0:
	   op1 = get_unibbles(1);
	   if(op1 != 0xe)
	    {
		 strcpy(l_instr, fixed_0[op1]);
		 return;
	    }
	   op2 = get_unibbles(1);
	   op3 = get_unibbles(1);
	   sprintf(l_instr, "%s   %s", fixed_0e[op3], std_fields[op2 & 017]);
	   return;
	   
      case 1:
	   decode_group_1();
	   return;
	   
      case 2:
	   op2 = get_unibbles(1);
	   sprintf(l_instr, "P=      %d", op2);
	   return;
	   
      case 3:
	   op2 = get_unibbles(1);
	   if(op2>4)
		sprintf(l_instr, "LCHEX   %s",make_hexstr_rev(op2+1));
	   else
	    {
		 op3 = get_unibbles(op2 + 1);
		 if(op2==4)
		      if(last_apc == org_pc-4)
		       {
			    add_local_symbol(org_pc+op3,T_SYM_NOHP);
			    sprintf(l_instr, "LC(5)   (%s)-(*)",
				    symbolic(org_pc+op3,DATAREL,SCODE));
		       }
		      else
			   sprintf(l_instr, "LC(5)   %s",
				   cksymbolic(op3,DATAREL, SCODE));
		 else
		      sprintf(l_instr, "LC(%d)   #%X", op2+1, op3);
		 if( (op2 == 1 ) && (op3 >= ' ') && ( op3 <= 127 ))
		      force_comment("LCASC '%c'", op3);
	    }
	   return;

      case 4:
	   op2 = get_nibbles(2);
	   if(op2 == 0x02)
		strcpy(l_instr, "NOP3");
	   else
		if(op2)
		 {
		      sprintf(l_instr, "GOC     %s",
			      symbolic((org_pc+1+op2) & 0xfffff, JUMPREL, SCODE));
		      comment_jump(org_pc+1+op2);
		      if(opt_formats && pass == PASSF)
			   add_auto_format(org_pc + 1 + op2, T_FMT_CODE);
		 }
		else
		     strcpy(l_instr, "RTNC");
	   return;
	   
      case 5:
           op2 = get_nibbles(2);
	   if(op2)
	    {
		 sprintf(l_instr, "GONC    %s",
			 symbolic((org_pc+1+op2) & 0xfffff, JUMPREL, SCODE));
		 comment_jump(org_pc+1+op2);
		 if(opt_formats && pass == PASSF)
		      add_auto_format(org_pc + 1 + op2, T_FMT_CODE);
	    }
	   else
		strcpy(l_instr, "RTNNC");
	   return;
	   
      case 6:
	   op2 = get_nibbles(3);
	   if(op2 == 0x003)
		strcpy(l_instr, "NOP4");
	   else if(op2 == 0x004)
	    {
		 get_unibbles(1);
		 strcpy(l_instr, "NOP5");
	    }
	   else
	    {
		 sprintf(l_instr, "GOTO    %s",
			 symbolic((org_pc+1+op2) & 0xfffff, JUMPREL, SCODE));
		 comment_jump((org_pc+1+op2) & 0xfffff);
		 if(opt_formats && pass == PASSF)
		      add_auto_format(org_pc + 1 + op2, T_FMT_CODE);
	    }
	   return;
	   
      case 7:
	   op2 = get_nibbles(3);
	   sprintf(l_instr, "GOSUB   %s",
		   symbolic((org_pc+4+op2) & 0xfffff, JUMPREL, SCODE));
	   gosub=TRUE;
	   comment_jump(org_pc+4+op2);
	   if(opt_formats && pass == PASSF)
		add_auto_format(org_pc + 4 + op2, T_FMT_CODE);
	   return;
	   
      default:
	   decode_8_thru_f(op0);
	   return;
      }
}

