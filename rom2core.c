#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>

int main(argc, argv)
    int argc;
    char **argv;
{
     int value, chr;
     FILE *in, *out;

     value = 0;

     if(!(in=fopen("rom.bin","r")))
      {
           perror("rom.bin");
           exit(1);
      }
     if(!(out=fopen("rom.core","w")))
      {
           perror("rom.core");
           exit(1);
      }
     while( (chr = getc(in) ) != EOF)
      {
                 value= (chr/16) & 0xF;
                 putc(value,out);
                 value=(chr) & 0xF ;
                 putc(value,out);
      }
     return 0 ;
}
